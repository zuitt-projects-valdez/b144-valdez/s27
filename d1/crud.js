const http = require("http");

//mock database - located outside
let directory = [
  {
    'name': "Brandon",
    'email': "brandon@mail.com",
  },
  {
    'name': "Jobert",
    'email': "jobert@mail.com",
  },
];

http
  .createServer((request, response) => {
    //Route for returning all items upon receiving a get request
    if (request.url == "/users" && request.method == "GET") {
      //retrieves resources like databases; gets the data being used; requests data from a specified resource
      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(directory));
      //write - what we want to give to the client
      //stringify because we want to convert the directory - sending to a client = stringified json
      //after client received the stringified json, will convert back to json object
      //input data type has to be a string
      response.end();
    }
    if (request.url == "/users" && request.method == "POST") {
      //declare request body for an empty string - container for user input data
      // declare and initialize 'requestBody' variable to an empty string - acts as a placeholder for resources/data to be created later on
      let requestBody = ""; //placeholder for received user data

      //stream = sequence of data on how to process the received user data
      //1. data stream - data is received from the client and is processed in the 'data' stream called 'data'the code below will be triggered
      //data step - reads the 'data' stream and process it as the request body
      //.on() - similar to eventListener - when data is given will run the function
      request.on("data", function (data) {
        //assign the data retrieved from the data stream to requestBody
        requestBody += data; //passes user data into the requestBody empty string
      }); //'data' represents the userdata received in the requestBody; this steps is what allows that

      //2. end step - only runs after the request has completely been sent; usually also the response once the data is already processed
      request.on("end", function () {
        //check if at this point, the request body is of data type string
        //need this to be of data type JSON to access its properties
        console.log(typeof requestBody);
        //convert the string requestBody to JSON - parse (client to server)
        requestBody = JSON.parse(requestBody);
        //create a new object representing the new mock database record
        let newUser = {
          'name': requestBody.name,
          'email': requestBody.email,
        };
        //add newUser to database
        directory.push(newUser) //.push - array of objects
        console.log(directory);
        //at this point, still requests but have not responded yet 
        response.writeHead(200, {'Content-Type': 'application/json'})
        response.write(JSON.stringify(newUser));
        response.end()
      });
    }
  })
  .listen(4000);

console.log(`Server running at localhost:4000`);
