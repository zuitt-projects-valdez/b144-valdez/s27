const http = require("http");
const port = 4000;
const server = http.createServer((req, res) => {
  // the http method of the incoming request can be accessed via the 'method' property of the 'request' parameter
  //GET method - will be retrieving or reading information; the default for http is the get method but indicate anyway to specify the method
  if (req.url == "/items" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Data retrieved from the database");
  }

  if (req.url == "/items" && req.method == "POST") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Data to be sent to the database");
  }

  if (req.url == "/updateItem" && req.method == "PUT") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Update our resources");
  }
  if (req.url == "/deleteItem" && req.method == "DELETE") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Delete our resources");
  }
  // method will still work despite having the same url because they are different methods anyway
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}`);

//npm install -g nodemon: installs nodemon globally so don't need to reinstall everywhere; only need to do once; install again incase it doesn't work
